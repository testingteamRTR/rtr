from tkinter import *
from math import *
from fractions import Fraction
import sys
from sys import stdin, __stdin__
class Calculadora():

    def bclick(self, num):
        global operator
        global a
        operator += str(num)
        a.set(operator)

    def del_num(self):
        global operator
        global a
        operator = operator[:-1]
        a.set(operator)

    def clear(self):
        global operator
        global a
        operator = ''
        a.set(operator)

    def save_results(self):
        global a
        global lista
        a.set(lista)


    def operation(self):
        global operator
        global a
        global lista
        try:
            print(operator)
            opera=eval(operator)
            if len(lista) < 4:
                lista.append(opera)
            if len(lista) == 4:
                lista.pop(0)
                lista.append(opera)
        except:
            self.clear()
            opera=('ERROR')
        a.set(opera)

class Converter():

    def bclick(self, num):
        global operator
        global a
        operator += str(num)
        a.set(operator)

    def ptomm(self):
        global operator
        global a
        operator = float(operator)*25.4
        a.set(operator)

    def ptocm(self):
        global operator
        global a
        operator = float(operator)*2.54
        a.set(operator)

    def ptom(self):
        global operator
        global a
        operator = float(operator)/39.37
        a.set(operator)

    def ptokm(self):
        global operator
        global a
        operator = float(operator)/39370.079
        a.set(operator)

    def mmtop(self):
        global operator
        global a
        operator = float(operator)/25.4
        a.set(operator)

    def mmtocm(self):
        global operator
        global a
        operator = float(operator)/10
        a.set(operator)

    def mmtom(self):
        global operator
        global a
        operator = float(operator)/1000
        a.set(operator)

    def mmtokm(self):
        global operator
        global a
        operator = float(operator)/1000000
        a.set(operator)

    def cmtop(self):
        global operator
        global a
        operator = float(operator)/2.54
        a.set(operator)

    def cmtomm(self):
        global operator
        global a
        operator = float(operator)*10
        a.set(operator)

    def cmtom(self):
        global operator
        global a
        operator = float(operator)/100
        a.set(operator)

    def cmtokm(self):
        global operator
        global a
        operator = float(operator)/100000
        a.set(operator)

    def mtop(self):
        global operator
        global a
        operator = float(operator)*39.37
        a.set(operator)

    def mtomm(self):
        global operator
        global a
        operator = float(operator)*1000
        a.set(operator)

    def mtocm(self):
        global operator
        global a
        operator = float(operator)*100
        a.set(operator)

    def mtokm(self):
        global operator
        global a
        operator = float(operator)/1000
        a.set(operator)

    def kmtop(self):
        global operator
        global a
        operator = float(operator)*39370.079
        a.set(operator)

    def kmtomm(self):
        global operator
        global a
        operator = float(operator)*1000000
        a.set(operator)

    def kmtocm(self):
        global operator
        global a
        operator = float(operator)*100000
        a.set(operator)

    def kmtom(self):
        global operator
        global a
        operator = float(operator)*1000
        a.set(operator)

    def gctogf(self):
        global operator
        global a
        operator = (float(operator)*(9/5))+32
        a.set(operator)

    def gctok(self):
        global operator
        global a
        operator = float(operator)+273.15
        a.set(operator)

    def gftogc(self):
        global operator
        global a
        operator = (float(operator)-32)*(5/9)
        a.set(operator)

    def gftok(self):
        global operator
        global a
        operator = (float(operator)-32)*(5/9)+273.15
        a.set(operator)

    def ktogc(self):
        global operator
        global a
        operator = (float(operator)-273.15)
        a.set(operator)

    def ktogf(self):
        global operator
        global a
        operator = (float(operator)-273.15)*(9/5)+32
        a.set(operator)

    def gtokg(self):
        global operator
        global a
        operator = float(operator)/1000
        a.set(operator)

    def gtoton(self):
        global operator
        global a
        operator = float(operator)/1000000
        a.set(operator)

    def gtolib(self):
        global operator
        global a
        operator = float(operator)/453.592
        a.set(operator)

    def kgtog(self):
        global operator
        global a
        operator = float(operator)*1000
        a.set(operator)

    def kgtoton(self):
        global operator
        global a
        operator = float(operator)/1000
        a.set(operator)

    def kgtolib(self):
        global operator
        global a
        operator = float(operator)*2.205
        a.set(operator)

    def tontog(self):
        global operator
        global a
        operator = float(operator)*1000000
        a.set(operator)

    def tontokg(self):
        global operator
        global a
        operator = float(operator)*1000
        a.set(operator)

    def tontolib(self):
        global operator
        global a
        operator = float(operator)*2204.623
        a.set(operator)

    def libtog(self):
        global operator
        global a
        operator = float(operator)*453.592
        a.set(operator)

    def libtokg(self):
        global operator
        global a
        operator = float(operator)/2.205
        a.set(operator)

    def libtoton(self):
        global operator
        global a
        operator = float(operator)/2204.623
        a.set(operator)

    def cm3_to_m3(self):
        global operator
        global a
        operator = float(operator)*(1/1000000)
        a.set(operator)

    def cm3_to_l(self):
        global operator
        global a
        operator = float(operator)*(1/1000)
        a.set(operator)

    def cm3_to_Gal(self):
        global operator
        global a
        operator = float(operator)/(3785.412)
        a.set(operator)

    def m3_to_cm3(self):
        global operator
        global a
        operator = float(operator)*(1000000)
        a.set(operator)

    def m3_to_l(self):
        global operator
        global a
        operator = float(operator)*(1000)
        a.set(operator)

    def m3_to_Gal(self):
        global operator
        global a
        operator = float(operator)*(264.172)
        a.set(operator)

    def l_to_cm3(self):
        global operator
        global a
        operator = float(operator)*(1000)
        a.set(operator)

    def l_to_m3(self):
        global operator
        global a
        operator = float(operator)/(1000)
        a.set(operator)

    def l_to_Gal(self):
        global operator
        global a
        operator = float(operator)/(0.264172)
        a.set(operator)

    def Gal_to_cm3(self):
        global operator
        global a
        operator = float(operator)*(3785.412)
        a.set(operator)

    def Gal_to_m3(self):
        global operator
        global a
        operator = float(operator)/(264.172)
        a.set(operator)

    def Gal_to_l(self):
        global operator
        global a
        operator = float(operator)*(3.785)
        a.set(operator)

    def pa_to_atm(self):
        global operator
        global a
        operator = float(operator)/(101325)
        a.set(operator)

    def pa_to_torr(self):
        global operator
        global a
        operator = float(operator)/(133.322)
        a.set(operator)

    def atm_to_pa(self):
        global operator
        global a
        operator = float(operator)*(101325)
        a.set(operator)

    def atm_to_torr(self):
        global operator
        global a
        operator = float(operator)*(760)
        a.set(operator)

    def torr_to_pa(self):
        global operator
        global a
        operator = float(operator)/(133.322)
        a.set(operator)

    def torr_to_atm(self):
        global operator
        global a
        operator = float(operator)*(760)
        a.set(operator)

print("1 PARA INICIAR LA CALCULADORA, 2 PARA INICIAR EL CONVERTIDOR: ")
option = sys.stdin.readline()
option = int(option)
if option == 1 or option == 2:
    if option == int(1):
        print("opcion 1")
        smart = Calculadora()
        operator = ''
        windows = Tk()
        a = StringVar()
        lista = []
        windows.geometry("445x489") #Dimensión de la ventana
        windows.title("calculator scientific")
        windows.resizable(False, False)
        buttonsqrt = Button(windows, text='2√', width = 11, height = 3, command=lambda:smart.bclick("sqrt("))
        buttonsqrt.place(x=1, y=85)
        buttonEXP = Button(windows, text='EXP', width = 11, height = 3, command=lambda:smart.bclick("**"))
        buttonEXP.place(x=90, y=85)
        buttonlog = Button(windows, text='log', width = 11, height = 3, command=lambda:smart.bclick("log10("))
        buttonlog.place(x=179, y=85)
        buttonln = Button(windows, text='ln', width = 11, height = 3, command=lambda:smart.bclick("log"))
        buttonln.place(x=268, y=85)
        buttone = Button(windows, text='e', width = 11, height = 3, command=lambda:smart.bclick("e"))
        buttone.place(x=357, y=85)
        buttonper=Button(windows, text="%", width = 11, height= 3, command=lambda:smart.bclick("/100")).place(x=0,y=144)
        buttonconv = Button(windows, text='conv', width = 11, height = 3, command=lambda:bclick("Fraction("))
        buttonconv.place(x=90, y=144)
        buttonconv = Button(windows, text='√', width = 11, height = 3, command=lambda:smart.bclick("**(1/"))
        buttonconv.place(x=180, y=144)
        buttonparleft=Button(windows, text="(", width = 11, height= 3, command=lambda:smart.bclick("("))
        buttonparleft.place(x=268, y=144)
        buttonparright=Button(windows, text=")", width = 11, height = 3, command=lambda:smart.bclick(")"))
        buttonparright.place(x=357, y=144)
        button7=Button(windows, text="7", width = 11, height = 3 , command=lambda:smart.bclick(7))
        button7.place(x=1, y=203)
        button8=Button(windows, text="8", width = 11, height = 3 , command=lambda:smart.bclick(8))
        button8.place(x=90, y=203)
        button9=Button(windows, text="9", width = 11, height = 3 , command=lambda:smart.bclick(9))
        button9.place(x=179, y=203)
        buttonAC=Button(windows, text="AC", width = 11, height = 3 , command=smart.clear)
        buttonAC.place(x=268, y=203)
        buttonDEL=Button(windows, text="DEL", width = 11, height = 3 , command=smart.del_num)
        buttonDEL.place(x=357, y=203)
        button4=Button(windows, text="4", width = 11 , height = 3 , command=lambda:smart.bclick(4))
        button4.place(x=1, y=262)
        button5=Button(windows, text="5", width = 11 , height = 3 , command=lambda:smart.bclick(5))
        button5.place(x=90, y=262)
        button6=Button(windows, text="6", width = 11 , height = 3 , command=lambda:smart.bclick(6))
        button6.place(x=179, y=262)
        buttonmult=Button(windows, text="x", width = 11 , height = 3 , command=lambda:smart.bclick("*"))
        buttonmult.place(x=268, y=262)
        buttondiv=Button(windows, text="÷", width = 11 , height = 3 , command=lambda:smart.bclick("/"))
        buttondiv.place(x=357, y=262)
        button1=Button(windows, text="1", width = 11, height= 3, command=lambda:smart.bclick(1))
        button1.place(x=1, y=321)
        button2=Button(windows, text="2", width = 11, height= 3, command=lambda:smart.bclick(2))
        button2.place(x=90, y=321)
        button3=Button(windows, text="3", width = 11, height= 3, command=lambda:smart.bclick(3))
        button3.place(x=179, y=321)
        buttonsum=Button(windows, text="+", width = 11, height= 3, command=lambda:smart.bclick("+"))
        buttonsum.place(x=268, y=321)
        buttonsub=Button(windows, text="-", width = 11, height= 3, command=lambda:smart.bclick("-"))
        buttonsub.place(x=357, y=321)
        button0=Button(windows, text="0", width = 11, height= 3, command=lambda:smart.bclick(0))
        button0.place(x=1, y=380)
        buttoncom=Button(windows, text=",", width = 11, height= 3, command=lambda:smart.bclick("."))
        buttoncom.place(x=90, y=380)
        buttonpi=Button(windows, text="π", width = 11, height= 3, command=lambda:smart.bclick("pi"))
        buttonpi.place(x=179, y=380)
        buttonres=Button(windows, text="=", width = 24, height= 3, command=smart.operation)
        buttonres.place(x=268, y=380)
        buttonResults=Button(windows, text="Resultados", width = 11, height= 3, command=smart.save_results)
        buttonResults.place(x=1, y=439)
        exit=Entry(windows, font=("Arial",20,"bold"), textvariable=a, width=22, bd=20, insertwidth=5, bg="powder blue", justify="right")
        exit.pack()
        windows.mainloop()
    elif option == 2:
        print("opcion 2")
        convertir = Converter()
        conver = Tk()
        operator = ''
        a = StringVar()
        conver.geometry("710x700")
        conver.title("convertidor")
        conver.resizable(False, False)
        #Conversión de pulgadas
        buttonPtomm = Button(conver, text='ptomm', width = 11, height = 3, command=convertir.ptomm)
        buttonPtomm.place(x=1, y=85)
        buttonPtocm = Button(conver, text='ptocm', width = 11, height = 3, command=convertir.ptocm)
        buttonPtocm.place(x=1, y=143)
        buttonPtom = Button(conver, text='ptom', width = 11, height = 3, command=convertir.ptom)
        buttonPtom.place(x=1, y=201)
        buttonPtokm = Button(conver, text='ptokm', width = 11, height = 3, command=convertir.ptokm)
        buttonPtokm.place(x=1, y=259)
        #Conversión de milimetros
        buttonMmtop = Button(conver, text='mmtop', width = 11, height = 3, command=convertir.mmtop)
        buttonMmtop.place(x=90, y=85)
        buttonMmtocm = Button(conver, text='mmtocm', width = 11, height = 3, command=convertir.mmtocm)
        buttonMmtocm.place(x=90, y=143)
        buttonMmtom = Button(conver, text='mmtom', width = 11, height = 3, command=convertir.mmtom)
        buttonMmtom.place(x=90, y=201)
        buttonMmtokm = Button(conver, text='mm to km', width = 11, height = 3, command=convertir.mmtokm)
        buttonMmtokm.place(x=90, y=259)
        #Conversión de centrimetros
        buttonCmtop = Button(conver, text='cmtop', width = 11, height = 3, command=convertir.cmtop)
        buttonCmtop.place(x=179, y=85)
        buttonCmtomm = Button(conver, text='cmtomm', width = 11, height = 3, command=convertir.cmtomm)
        buttonCmtomm.place(x=179, y=143)
        buttonCmtom = Button(conver, text='cmtom', width = 11, height = 3, command=convertir.cmtom)
        buttonCmtom.place(x=179, y=201)
        buttonCmtokm = Button(conver, text='cmtokm', width = 11, height = 3, command=convertir.cmtokm)
        buttonCmtokm.place(x=179, y=259)
        #Conversión de metros
        buttonMtop=Button(conver, text="mtop", width = 11, height= 3, command=convertir.mtop)
        buttonMtop.place(x=268, y=85)
        buttonMtomm=Button(conver, text="mtomm", width = 11, height= 3, command=convertir.mtomm)
        buttonMtomm.place(x=268, y=143)
        buttonMtocm=Button(conver, text="mtocm", width = 11, height = 3, command=convertir.mtocm)
        buttonMtocm.place(x=268, y=201)
        buttonMtokm=Button(conver, text="mtokm", width = 11, height = 3 , command=convertir.mtokm)
        buttonMtokm.place(x=268, y=259)
        #Conversión de kilometros
        buttonKmtop=Button(conver, text="kmtop", width = 11, height = 3 , command=convertir.kmtop)
        buttonKmtop.place(x=357, y=85)
        buttonKmtomm=Button(conver, text="kmtomm", width = 11, height = 3 , command=convertir.kmtomm)
        buttonKmtomm.place(x=357, y=143)
        buttonKmtocm=Button(conver, text="kmtocm", width = 11, height = 3 , command=convertir.kmtocm)
        buttonKmtocm.place(x=357, y=201)
        buttonKmtom=Button(conver, text="kmtom", width = 11, height = 3 , command=convertir.kmtom)
        buttonKmtom.place(x=357, y=259)
        #Conversión de Grados Celsius
        buttonGCtoGF=Button(conver, text="GCtoGF", width = 11, height = 3 , command=convertir.gctogf)
        buttonGCtoGF.place(x=268, y=317)
        buttonGCtoK=Button(conver, text="GCtoK", width = 11, height = 3 , command=convertir.gctok)
        buttonGCtoK.place(x=357, y=317)
        #Conversión de Grados Fahrenheit
        buttonGFtoGC=Button(conver, text="GFtoGC", width = 11, height = 3 , command=convertir.gftogc)
        buttonGFtoGC.place(x=268, y=375)
        buttonGFtoK=Button(conver, text="GFtoK", width = 11, height = 3 , command=convertir.gftok)
        buttonGFtoK.place(x=357, y=375)
        #Conversión de Kelvin
        buttonKtoGC=Button(conver, text="KtoGC", width = 11, height = 3 , command=convertir.ktogc)
        buttonKtoGC.place(x=268, y=433)
        buttonKtoGF=Button(conver, text="KtoGF", width = 11, height = 3 , command=convertir.ktogf)
        buttonKtoGF.place(x=357, y=433)
        #Conversión de Gramos
        buttongtokg=Button(conver, text="GtoKG", width = 11, height = 3 , command=convertir.gtokg)
        buttongtokg.place(x=1, y=317)
        buttongtoton=Button(conver, text="GtoTON", width = 11, height = 3 , command=convertir.gtoton)
        buttongtoton.place(x=90, y=317)
        buttongtolib=Button(conver, text="GtoLIB", width = 11, height = 3 , command=convertir.gtolib)
        buttongtolib.place(x=179, y=317)
        #Conversión de Kilogramos
        buttonkgtog=Button(conver, text="KGtoG", width = 11, height = 3 , command=convertir.kgtog)
        buttonkgtog.place(x=1, y=375)
        buttonkgtoton=Button(conver, text="KGtoTON", width = 11, height = 3 , command=convertir.kgtoton)
        buttonkgtoton.place(x=90, y=375)
        buttonkgtolib=Button(conver, text="KGtoLIB", width = 11, height = 3 , command=convertir.kgtolib)
        buttonkgtolib.place(x=179, y=375)
        #Conversión de Toneladas
        buttontontog=Button(conver, text="TONtoG", width = 11, height = 3 , command=convertir.tontog)
        buttontontog.place(x=1, y=433)
        buttontontokg=Button(conver, text="TONtoKG", width = 11, height = 3 , command=convertir.tontokg)
        buttontontokg.place(x=90, y=433)
        buttontontolib=Button(conver, text="TONtoLIB", width = 11, height = 3 , command=convertir.tontolib)
        buttontontolib.place(x=179, y=433)
        #Conversión de libras
        buttonlibtog=Button(conver, text="LIBtoG", width = 11, height = 3 , command=convertir.libtog)
        buttonlibtog.place(x=1, y=433)
        buttonlibtokg=Button(conver, text="LIBtoKG", width = 11, height = 3 , command=convertir.libtokg)
        buttonlibtokg.place(x=90, y=433)
        buttonlibtoton=Button(conver, text="LIBtoTON", width = 11, height = 3 , command=convertir.libtoton)
        buttonlibtoton.place(x=179, y=433)
        #Conversión de cm3
        buttoncm3tom3=Button(conver, text="cm3 -> m3", width = 11, height = 3 , command=convertir.cm3_to_m3)
        buttoncm3tom3.place(x=1, y=491)
        buttoncm3tol=Button(conver, text="cm3 -> L", width = 11, height = 3 , command=convertir.cm3_to_l)
        buttoncm3tol.place(x=90, y=491)
        buttoncm3togal=Button(conver, text="cm3 -> Gal", width = 11, height = 3 , command=convertir.cm3_to_Gal)
        buttoncm3togal.place(x=179, y=491)
        #Conversión de M3
        buttonm3tocm3=Button(conver, text="m3 -> cm3", width = 11, height= 3, command=convertir.m3_to_cm3)
        buttonm3tocm3.place(x=268, y=491)
        buttonm3tol=Button(conver, text="m3 -> L", width = 11, height= 3, command=convertir.m3_to_l)
        buttonm3tol.place(x=357, y=491)
        buttonm3toGal=Button(conver, text="m3 -> Gal", width = 11, height = 3, command=convertir.m3_to_Gal)
        buttonm3toGal.place(x=446, y=491)
        #Conversión de L
        buttonltocm3=Button(conver, text="L -> cm3", width = 11, height = 3 , command=convertir.l_to_cm3)
        buttonltocm3.place(x=1, y=549)
        buttonltom3=Button(conver, text="L -> m3", width = 11, height = 3 , command=convertir.l_to_m3)
        buttonltom3.place(x=90, y=549)
        buttonltoGal=Button(conver, text="L -> Gal", width = 11, height = 3 , command=convertir.l_to_Gal)
        buttonltoGal.place(x=179, y=549)
        #Conversión de Galones
        buttonGaltocm3=Button(conver, text="Gal -> cm3", width = 11, height= 3, command=convertir.Gal_to_cm3)
        buttonGaltocm3.place(x=268, y=549)
        buttonGaltim3=Button(conver, text="Gal -> m3", width = 11, height= 3, command=convertir.Gal_to_m3)
        buttonGaltim3.place(x=357, y=549)
        buttonGaltol=Button(conver, text="Gal -> L", width = 11, height = 3, command=convertir.Gal_to_l)
        buttonGaltol.place(x=446, y=549)
        # conversión de pascal
        buttonpatoatm=Button(conver, text="Pa -> atm", width = 11, height = 3 , command=convertir.pa_to_atm)
        buttonpatoatm.place(x=1, y=549)
        buttonpatotorr=Button(conver, text="Pa -> Torr", width = 11, height = 3 , command=convertir.pa_to_torr)
        buttonpatotorr.place(x=90, y=549)
        # conversion de ATM
        buttonatmtopa=Button(conver, text="atm -> Pa", width = 11, height = 3 , command=convertir.atm_to_pa)
        buttonatmtopa.place(x=179, y=549)
        buttonatmtotorr=Button(conver, text="atm -> Torr", width = 11, height= 3, command=convertir.atm_to_torr)
        buttonatmtotorr.place(x=268, y=549)
        # conversión de Torr
        buttontorrtopa=Button(conver, text="Torr -> Pa", width = 11, height= 3, command=convertir.torr_to_pa)
        buttontorrtopa.place(x=357, y=549)
        buttontorrtoatm=Button(conver, text="Torr -> atm", width = 11, height = 3, command=convertir.torr_to_atm)
        buttontorrtoatm.place(x=446, y=549)
        #Botones de numeros
        button0=Button(conver, text="0", width = 11, height= 3, command=lambda:convertir.bclick(0))
        button0.place(x=446, y=259)
        button1=Button(conver, text="1", width = 11, height= 3, command=lambda:convertir.bclick(1)) 
        button1.place(x=446, y=201)
        button2=Button(conver, text="2", width = 11, height= 3, command=lambda:convertir.bclick(2))
        button2.place(x=535, y=201)
        button3=Button(conver, text="3", width = 11, height= 3, command=lambda:convertir.bclick(3))
        button3.place(x=624, y=201)
        button4=Button(conver, text="4", width = 11 , height = 3 , command=lambda:convertir.bclick(4))
        button4.place(x=446, y=143)
        button5=Button(conver, text="5", width = 11 , height = 3 , command=lambda:convertir.bclick(5))
        button5.place(x=535, y=143)
        button6=Button(conver, text="6", width = 11 , height = 3 , command=lambda:convertir.bclick(6))
        button6.place(x=624, y=143)
        button7=Button(conver, text="7", width = 11, height = 3 , command=lambda:convertir.bclick(7))
        button7.place(x=446, y=85)
        button8=Button(conver, text="8", width = 11, height = 3 , command=lambda:convertir.bclick(8))
        button8.place(x=535, y=85)
        button9=Button(conver, text="9", width = 11, height = 3 , command=lambda:convertir.bclick(9))
        button9.place(x=624, y=85)
        exit=Entry(conver, font=("Arial",20,"bold"), textvariable=a, width=22, bd=20, insertwidth=5, bg="powder blue", justify="right")
        exit.pack()
        conver.mainloop()
